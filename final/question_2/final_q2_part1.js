use enron;
db.messages.aggregate([
    {$unwind: '$headers.To'},
    {$group: {_id : '$_id', From : {$first: '$headers.From'}, To: {$addToSet : '$headers.To'}}}
])

//Part1
//For this question you will use the aggregation framework to figure out pairs of people that tend to communicate a lot.
//    To do this, you will need to unwind the To list for each message.

//Davids-MacBook-Pro:question_2 davidtan$ ls
//aggregation_final_q2    final_q2.js             instructions
//Davids-MacBook-Pro:question_2 davidtan$ cat final_q2.js | mongo
//MongoDB shell version: 2.6.6
//connecting to: test
//switched to db enron
//{ "_id" : ObjectId("4f16fe39d1e2d3237107e4e9"), "From" : "john.zufferli@enron.com", "To" : [ "kate.joslyn@enron.com" ] }
//{ "_id" : ObjectId("4f16fe39d1e2d3237107e4e8"), "From" : "john.zufferli@enron.com", "To" : [ "clickathomecanada@enron.com" ] }
//{ "_id" : ObjectId("4f16fe39d1e2d3237107e4e7"), "From" : "john.zufferli@enron.com", "To" : [ "kimberly.hillis@enron.com" ] }
//{ "_id" : ObjectId("4f16fe39d1e2d3237107e4e6"), "From" : "john.zufferli@enron.com", "To" : [ "bill.greenizan@enron.com", "cooper.richey@enron.com" ] }
//{ "_id" : ObjectId("4f16fe39d1e2d3237107e4e5"), "From" : "john.zufferli@enron.com", "To" : [ "robert.hemstock@enron.com" ] }
//{ "_id" : ObjectId("4f16fe39d1e2d3237107e4e4"), "From" : "john.zufferli@enron.com", "To" : [ "john.lavorato@enron.com" ] }
//{ "_id" : ObjectId("4f16fe39d1e2d3237107e4e3"), "From" : "john.zufferli@enron.com", "To" : [ "kimberly.hillis@enron.com" ] }
//{ "_id" : ObjectId("4f16fe39d1e2d3237107e4e2"), "From" : "john.zufferli@enron.com", "To" : [ "john.lavorato@enron.com" ] }
//{ "_id" : ObjectId("4f16fe39d1e2d3237107e4e0"), "From" : "john.zufferli@enron.com", "To" : [ "jonathan.mckay@enron.com" ] }
//{ "_id" : ObjectId("4f16fe39d1e2d3237107e4df"), "From" : "john.zufferli@enron.com", "To" : [ "jonathan.mckay@enron.com" ] }
//{ "_id" : ObjectId("4f16fe39d1e2d3237107e4de"), "From" : "john.zufferli@enron.com", "To" : [ "paul.leclair@home.com" ] }
//{ "_id" : ObjectId("4f16fe39d1e2d3237107e4dd"), "From" : "john.zufferli@enron.com", "To" : [ "rob.milnthorp@enron.com" ] }
//{ "_id" : ObjectId("4f16fe39d1e2d3237107e4da"), "From" : "john.zufferli@enron.com", "To" : [ "p..messenger@enron.com", "tyler.seminuk@enron.com", "michael.taylor@enron.com", "murray.fichten@enron.com" ] }
//{ "_id" : ObjectId("4f16fe39d1e2d3237107e4d9"), "From" : "john.zufferli@enron.com", "To" : [ "kimberly.hillis@enron.com" ] }
//{ "_id" : ObjectId("4f16fe39d1e2d3237107e4d8"), "From" : "john.zufferli@enron.com", "To" : [ "jzufferli@home.com" ] }
//{ "_id" : ObjectId("4f16fe39d1e2d3237107e4d4"), "From" : "john.zufferli@enron.com", "To" : [ "angela.mcculloch@enron.com" ] }
//{ "_id" : ObjectId("4f16fe39d1e2d3237107e4d3"), "From" : "john.zufferli@enron.com", "To" : [ "wconwell@rr.com" ] }
//{ "_id" : ObjectId("4f16fe39d1e2d3237107e4d2"), "From" : "john.zufferli@enron.com", "To" : [ "jzufferli@home.com" ] }
//{ "_id" : ObjectId("4f16fe39d1e2d3237107e4d1"), "From" : "john.zufferli@enron.com", "To" : [ "p..messenger@enron.com" ] }
//{ "_id" : ObjectId("4f16fe39d1e2d3237107e4d0"), "From" : "john.zufferli@enron.com", "To" : [ "john.postlethwaite@enron.com" ] }
//Type "it" for more
//    bye
//Davids-MacBook-Pro:question_2 davidtan$
