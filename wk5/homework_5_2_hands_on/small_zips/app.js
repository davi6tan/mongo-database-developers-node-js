use test;
db.zips.aggregate(
    { $match:
    //{ state: { $in: [ 'CT', 'NJ' ] }}},
    { state: { $in: [ 'CA', 'NY' ] }}},
    { $group:
    { _id: { state: "$state", city: "$city" },
        pop: { $sum: "$pop" }}},
    { $match:
    { pop: { $gt: 25000 }}},
    { $group:
    { _id: "combo",
        avgCityPop: { $avg: "$pop" }}}
);

//
// Davids-MacBook-Pro:small_zips davidtan$ ls
// app.js          notes           small_zips.json
// Davids-MacBook-Pro:small_zips davidtan$ cat app.js | mongo
// MongoDB shell version: 2.6.6
// connecting to: test
// switched to db test
// { "_id" : "combo", "avgCityPop" : 44804.782608695656 }
// bye
// Davids-MacBook-Pro:small_zips davidtan$
//
//
//for CT, NJ
// Davids-MacBook-Pro:small_zips davidtan$ cat app.js | mongo
// MongoDB shell version: 2.6.6
// connecting to: test
// switched to db test
// { "_id" : "combo", "avgCityPop" : 38176.63636363636 }

